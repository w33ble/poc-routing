import test from 'ava';
import createRouter from './router';

test('throws if routes is not an array', t => {
  const check = () => createRouter('hello world');
  t.throws(check);
});

test('throws on path only', t => {
  // action or children is required
  const routes = [{ path: '/test' }];
  const check = () => createRouter(routes);
  t.throws(check);
});

test('throws on children with path only', t => {
  // action or children is required
  const routes = [
    {
      path: '/test',
      children: [
        {
          path: '/',
        },
      ],
    },
  ];

  const check = () => createRouter(routes);
  t.throws(check);
});

test('throws with invalid path', t => {
  // routes must contain a leading slash
  const routes = [{ path: 'test', action: () => {} }];
  const check = () => createRouter(routes);
  t.throws(check);
});

test('throws with duplicate names', t => {
  // named routes must have unique names
  const routes = [
    { name: 'testroute', path: '/test1', action: () => {} },
    { name: 'testroute', path: '/test2', action: () => {} },
  ];
  const check = () => createRouter(routes);
  t.throws(check);
});

test('throws with duplicate names in children', t => {
  const routes = [
    { name: 'testroute', path: '/test1', action: () => {} },
    {
      path: '/test2',
      children: [{ name: 'testroute', path: '/', action: () => {} }],
    },
  ];
  const check = () => createRouter(routes);
  t.throws(check);
});

test('router api', t => {
  const routes = [{ path: '/test', action: () => {} }];
  const router = createRouter(routes);
  t.true(typeof router.match === 'function');
  t.true(typeof router.parse === 'function');
  t.true(typeof router.create === 'function');
});

test('match resolves routes correctly', t => {
  const routes = [
    { path: '/test', action: () => {} },
    { path: '/test2', name: 'test2', action: () => {} },
    { path: '/path/:with/:params', action: () => {} },
    { path: '/path2/:with/:params', name: 'path2', action: () => {} },
  ];
  const router = createRouter(routes);

  // test non-matching route
  t.false(router.match('/hello/world'));

  // test simple routes, checking name property
  t.deepEqual(router.match('/test'), { path: routes[0].path, name: routes[0].name });
  t.deepEqual(router.match('/test2'), { path: routes[1].path, name: routes[1].name });

  // test param route
  t.deepEqual(router.match('/path/to/winning'), { path: routes[2].path, name: routes[2].name });
  t.deepEqual(router.match('/path2/all/green'), { path: routes[3].path, name: routes[3].name });
});

test('parse resolves params correctly', async t => {
  const routes = [{ path: '/path/:with/:params', action: () => {} }];
  const router = createRouter(routes);

  // test non-matching route
  const nomatched = await router.parse('/no/route');
  t.false(nomatched);

  // test param values
  const matched = await router.parse('/path/to/winning');
  t.deepEqual(matched.route, { path: routes[0].path, name: routes[0].name });
  t.deepEqual(matched.params, { with: 'to', params: 'winning' });

  // test casting number params to float
  const matched2 = await router.parse('/path/100/3.14159');
  t.deepEqual(matched2.route, { path: routes[0].path, name: routes[0].name });
  t.notDeepEqual(matched2.params, { with: '100', params: '3.14159' });
  t.deepEqual(matched2.params, { with: 100, params: 3.14159 });
});

test('parse calls matching route action', async t => {
  t.plan(2);
  const handler = async ({ route, params }) => {
    // test items passed to action
    t.deepEqual(route, { path: routes[0].path, name: routes[0].name });
    t.deepEqual(params, { with: 'to', params: 'winning' });
  };

  const routes = [{ path: '/path/:with/:params', action: handler }];
  const router = createRouter(routes);

  // execute route to call the action function
  await router.parse('/path/to/winning');
});

test('create should be false', t => {
  const routes = [
    { path: '/test', name: 'test', action: () => {} },
    { path: '/path/:with/:params', name: 'path', action: () => {} },
  ];
  const router = createRouter(routes);

  // returns false with missing or ivnalid name
  t.false(router.create());
  t.false(router.create('nope'));
});

test('create should throw with missing params', t => {
  const routes = [
    { path: '/test', name: 'test', action: () => {} },
    { path: '/path/:with/:params', name: 'path', action: () => {} },
  ];
  const router = createRouter(routes);

  // should throw unless given all route params
  t.throws(() => router.create('path'));
  t.throws(() => router.create('path', {}));
  t.throws(() => router.create('path', { with: 'hotsauce' }));
  t.throws(() => router.create('path', { params: 'cheese' }));
  t.notThrows(() => router.create('path', { with: 'hotsauce', params: 'cheese' }));
});

test('create should return constructed route', t => {
  const routes = [
    { path: '/test', name: 'test', action: () => {} },
    { path: '/path/:with/:params', name: 'path', action: () => {} },
  ];
  const router = createRouter(routes);

  // should create routes with and without params
  t.is(router.create('test'), '/test');
  t.is(router.create('path', { with: 1, params: 'two' }), '/path/1/two');
});
