export default function logger(...args) {
  const timestamp = new Date().toLocaleString();
  const argsString = args.reduce((acc, arg) => {
    return `${acc} ${JSON.stringify(arg)}`;
  }, '');

  const logLine = document.createElement('div');
  logLine.textContent = `[${timestamp}]: ${argsString}`;

  document.getElementById('logs').prepend(logLine);
}
