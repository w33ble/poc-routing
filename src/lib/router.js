import Path from 'path-parser';

// helper functions
const isObjectLike = val => typeof val === 'object' && val != null;

const hasKey = (obj, key) => obj.hasOwnProperty(key);

// path format helpers
const isValidRouteConfig = conf => {
  if (!isObjectLike(conf)) return false;
  return hasKey(conf, 'path') && (hasKey(conf, 'action') || hasKey(conf, 'children'));
};

const getPath = (path, appendTo = '') => {
  if (!/^\//.test(path)) return false;
  const newPath = `${appendTo.replace(/\/$/, '')}${path}`;
  return newPath.length > 1 ? newPath.replace(/\/$/, '') : newPath;
};

// given a route object and optional parent path, create an array of route configs
function parseRoute(route, path = '') {
  if (!isValidRouteConfig(route)) {
    throw new Error(`Route definition is invalid: ${JSON.stringify(route)}`);
  }

  const fullPath = getPath(route.path, path);
  if (fullPath === false) throw new Error(`Route's path is invalid: ${route.path}`);

  // if children is defined, recurse into child definitions, appending parent paths
  if (Array.isArray(route.children)) {
    return route.children.reduce((routesAcc, childRoute) => {
      return routesAcc.concat(parseRoute(childRoute, fullPath));
    }, []);
  }

  // return route object
  return [
    {
      path: fullPath,
      action: route.action,
      name: route.name,
      parser: new Path(fullPath),
    },
  ];
}

export default function createRouter(routes) {
  if (!Array.isArray(routes)) throw new Error('An array of route objects is required');

  // flatten routes into descrete objects and parsed paths
  const routeConfigs = routes.reduce(
    (acc, route) => {
      parseRoute(route).forEach(({ parser, ...routeConfig }) => {
        if (routeConfig.name != null && acc.names.includes(routeConfig.name)) {
          throw new Error(
            `Route with name '${routeConfig.name}' already defined, names must be unique`
          );
        }

        acc.parsers.push(parser);
        acc.configs.push(routeConfig);
        acc.names.push(routeConfig.name);
      });

      return acc;
    },
    { configs: [], parsers: [], names: [] }
  );

  function getRoute(url) {
    const index = routeConfigs.parsers.findIndex(parser => parser.test(url));
    return !~index ? false : { ...routeConfigs.configs[index], index };
  }

  return {
    match(url) {
      const matched = getRoute(url);

      // no match, nothing left to do
      if (matched === false) return false;

      const { path, name } = matched;
      return { path, name };
    },

    async parse(url) {
      const matched = getRoute(url);

      // no match, nothing left to do
      if (matched === false) return false;

      // parse params, cast any number values to numbers
      const params = routeConfigs.parsers[matched.index].test(url);
      Object.keys(params).forEach(key => {
        if (!isNaN(params[key])) params[key] = parseFloat(params[key]);
      });

      const payload = {
        route: { path: matched.path, name: matched.name },
        params,
      };

      // call the route handler, and return the handler payload on completion
      await matched.action(payload);
      return payload;
    },

    create(name, params = {}) {
      // only create routes from existing named routes
      if (name == null || !routeConfigs.names.includes(name)) return false;

      // build and return the route given using provided params
      // throws when a route can not be built
      const routeIndex = routeConfigs.configs.findIndex(config => config.name === name);
      return routeConfigs.parsers[routeIndex].build(params);
    },
  };
}
