import { createBrowserHistory, createMemoryHistory } from 'history';

function getHistoryInstance(win) {
  if (typeof win === 'undefined' || !win.history) {
    return createMemoryHistory();
  } else {
    // const basePath = chrome.getBasePath();
    const basename = '#/';

    // hacky fix for initial page load so basename matches with the hash
    if (win.location.hash === '') {
      win.history.replaceState({}, '', `${basename}`);
    }

    // if window object, create browser instance
    return createBrowserHistory({
      basename,
    });
  }
}

export default function createHistory(win) {
  const unlisteners = [];
  const history = getHistoryInstance(win);

  return {
    listen(fn) {
      const unlisten = history.listen((...args) => {
        fn(...args);
      });

      unlisteners.push(unlisten);
    },

    clearListeners() {
      unlisteners.forEach(unlisten => unlisten());
    },

    push(url, state) {
      history.push(url, state);
    },

    replace(url, state) {
      history.replace(url, state);
    },
  };
}
