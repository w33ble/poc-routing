function fnHome(route) {
  console.log('home route', route);
}

function fnWorkpad(route) {
  console.log('workpad route', route);
}

function fnCreateWorkpad(route) {
  console.log('workpad create route', route);
}

export default [
  {
    name: 'home',
    path: '/',
    action: fnHome,
  },
  // {
  //   name: 'workpad',
  //   path: '/workpads/:id/page/:page',
  //   action: fnWorkpad,
  // },
  {
    path: '/workpad',
    children: [
      {
        name: 'loadWorkpad',
        path: '/:id/page/:page',
        action: fnWorkpad,
      },
      {
        path: '/create',
        children: [
          {
            name: 'createWorkpad',
            path: '/',
            action: fnCreateWorkpad,
          },
          {
            name: 'createTemplatedWorkpad',
            path: '/templated',
            action: fnCreateWorkpad,
          },
        ],
      },
    ],
  },
];
