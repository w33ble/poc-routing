import React from 'react';
import Router from './Router';
import RouteTest from './RouteTest';
import logger from '../lib/logger';

export default class App extends React.PureComponent {
  componentDidMount() {
    logger('App component mounted');
  }

  render() {
    return (
      <div>
        <h1>Router PoC</h1>

        <h3>Tasks</h3>
        <ul>
          <li>Define route definition shape</li>
          <li>Create Link component</li>
          <li>Create Router component</li>
        </ul>

        <h3>Specs</h3>
        <ul>
          <li>
            [X] Should cast number values to Number -{' '}
            <a href="https://github.com/faceyspacey/redux-first-router/blob/master/src/pure-utils/pathToAction.js#L46-L50">
              redux-first-router
            </a>
          </li>
          <li>
            [X] Support nested children route matching ala{' '}
            <a href="https://github.com/kriasoft/universal-router#how-does-it-look-like">
              nested-router
            </a>{' '}
            and <a href="https://router.vuejs.org/en/essentials/nested-routes.html">vue-router</a>
          </li>
          <li>
            [X] Support named routes like{' '}
            <a href="https://router.vuejs.org/en/essentials/named-routes.html">vue-router</a>,
            useful for simpler navigation
          </li>
          <li>[X] Fail when multiple routes with the same name are added</li>
        </ul>

        <h3>Inspiration</h3>
        <ul>
          <li>
            <a href="https://router.vuejs.org/en/">vue-router</a> and{' '}
            <a href="https://github.com/kriasoft/universal-router">universal-router</a> for syntax
          </li>
          <li>
            <a href="https://github.com/download/uroute">uroute.js</a> and{' '}
            <a href="https://github.com/Frapwings/fendjs-route/blob/master/index.js">
              fendjs-route
            </a>{' '}
            for their simplicity
          </li>
        </ul>

        <Router>
          <RouteTest />
        </Router>
      </div>
    );
  }
}
