import React from 'react';
import PropTypes from 'prop-types';

export default class Link extends React.PureComponent {
  static propTypes = {
    name: PropTypes.string.isRequired,
    params: PropTypes.object,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.node]).isRequired,
  };

  static contextTypes = {
    router: PropTypes.object,
  };

  render() {
    const { name, params } = this.props;
    const link = this.context.router.create(name, params);

    return <a href={`#${link}`}>{this.props.children}</a>;
  }
}
