import React from 'react';
import PropTypes from 'prop-types';
import createRouter from '../lib/router';
import routes from '../routes';

export default class Router extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  static childContextTypes = {
    router: PropTypes.object,
  };

  static state = {
    router: {},
  };

  getChildContext() {
    return { router: this.state.router };
  }

  componentWillMount() {
    this.setState({ router: createRouter(routes) });
  }

  render() {
    return this.props.children;
  }
}
