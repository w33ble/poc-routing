import React from 'react';
import PropTypes from 'prop-types';
import Link from './Link';
import createHistory from '../lib/history';
import logger from '../lib/logger';

export default class RouteTest extends React.PureComponent {
  static contextTypes = {
    router: PropTypes.object,
  };

  componentDidMount() {
    this.history = createHistory(window);
    const { router } = this.context;

    this.history.listen(async (location, action) => {
      const { pathname, state } = location;
      const routePayload = await router.parse(pathname);
      logger('history change', action, { pathname, state }, routePayload);
    });
  }

  componentWillUnmount() {
    this.history.clearListeners();
  }

  createWorkpad = () => {
    this.history.push('/workpad/create', { hasState: true });
  };

  loadWorkpad = ({ id, page }) => {
    try {
      const route = this.context.router.create('loadWorkpad', { id, page });
      this.history.push(route, { hasState: true });
    } catch (e) {
      alert('navigation failed');
    }
  };

  render() {
    return (
      <div>
        <h3>hash based href links</h3>
        <div>
          <a href="#/">Home</a>
        </div>
        <div>
          <a href="#/workpad/create">Workpad Create</a>
        </div>
        <div>
          <a href="#/workpad/workpad-id-123/page/3">Workpad Load</a>
        </div>

        <h3>history based click handlers</h3>
        <div>
          <button onClick={this.createWorkpad}>Workpad Create, with state</button>
        </div>
        <div>
          <button onClick={() => this.loadWorkpad({ id: 'workpad-42', page: '13' })}>
            Navigate to workpad load
          </button>
        </div>

        <h3>Link component navigation</h3>
        <div>
          <Link name="home">Home via Link</Link>
        </div>
      </div>
    );
  }
}
