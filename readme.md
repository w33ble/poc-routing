# poc-routing

client-side routing PoC.

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/w33ble/poc-routing/master/LICENSE)

#### License

MIT © [w33ble](https://github.com/w33ble)